/**
 * @file   can_driver.c
 * @brief  CAN驱动程序
 * @author wdluo(wdluo@toomoss.com)
 * @version 1.0
 * @date 2022-10-18
 * @copyright Copyright (c) 2022 重庆图莫斯电子科技有限公司
 */
#include <stdint.h>
#include "main.h"
#include "can_boot.h"
#include "can_driver.h"
#include "can_uds.h"
#include "can_buffer.h"
//CAN波特率参数表
CAN_BAUDRATE  CAN_BaudRateInitTab[]= {      
   /*{1000000,CAN_SJW_1tq,CAN_BS1_2tq,CAN_BS2_1tq,21},   // 1M
   {500000,CAN_SJW_1tq,CAN_BS1_6tq,CAN_BS2_1tq,21},    // 500K
   {200000,CAN_SJW_1tq,CAN_BS1_4tq,CAN_BS2_1tq,70},    // 200K
   {100000,CAN_SJW_1tq,CAN_BS1_6tq,CAN_BS2_1tq,105},   // 100K
   {50000,CAN_SJW_1tq,CAN_BS1_6tq,CAN_BS2_1tq,210},    // 50K
   {20000,CAN_SJW_1tq,CAN_BS1_6tq,CAN_BS2_1tq,525},    // 20K
   {0,0,0,0,0}*/
};
extern FDCAN_HandleTypeDef hfdcan1;
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  从CAN波特率参数表里面获取CAN波特率参数
  * @param  BaudRate CAN波特率值，单位为bps
  * @param  pParaBaudRate CAN波特率参数结构体指针
  * @return 波特率参数获取状态状态
  * @retval 1 获取波特率参数成功
  * @retval 0 获取波特率参数失败
  */
uint8_t CAN_GetBaudRatePara(uint32_t BaudRate,CAN_BAUDRATE *pParaBaudRate)
{
  /*int i=0;
  while(CAN_BaudRateInitTab[i].BaudRate!=0){
    if(CAN_BaudRateInitTab[i].BaudRate==BaudRate){
      *pParaBaudRate = CAN_BaudRateInitTab[i];
      return 1;
    }
    i++;
  };*/
  return 0;
}

/**
  * @brief  配置CAN接收过滤器
  * @param  FilterNumber 过滤器号
  * @param  ReceiveMsgId 接收数据的ID
	* @param  ReceiveMsgIdType 接收数据的ID类型，0-标准帧，1-扩展帧
  * @retval None
  */
void CAN_ConfigFilter(uint8_t Channel,uint8_t FilterNumber,uint32_t ReceiveMsgId,uint8_t ReceiveMsgIdType)
{ 
  FDCAN_FilterTypeDef FDCAN1_RXFilter;
  FDCAN1_RXFilter.FilterID1=ReceiveMsgId;
  if(ReceiveMsgIdType == FDCAN_STANDARD_ID){
    FDCAN1_RXFilter.IdType=FDCAN_STANDARD_ID;
    FDCAN1_RXFilter.FilterID2=0x7FF;
  }else{
    FDCAN1_RXFilter.IdType=FDCAN_EXTENDED_ID;
    FDCAN1_RXFilter.FilterID2=0x1FFFFFFF;
  }
  FDCAN1_RXFilter.FilterIndex=FilterNumber;
  FDCAN1_RXFilter.FilterType=FDCAN_FILTER_MASK;
  FDCAN1_RXFilter.FilterConfig=FDCAN_FILTER_TO_RXFIFO0;
  
  if(HAL_FDCAN_ConfigFilter(&hfdcan1,&FDCAN1_RXFilter)!=HAL_OK){
    printf("Config CAN Faild!\r\n");
    return;
  }
  return;
}
/**
 * @brief  
 * @param  BaudRate: 
 */
void CAN_Configuration(uint8_t Channel,uint32_t BaudRate)
{
  hfdcan1.Instance = FDCAN1;
  hfdcan1.Init.FrameFormat = FDCAN_FRAME_CLASSIC;
  hfdcan1.Init.Mode = FDCAN_MODE_NORMAL;
  hfdcan1.Init.AutoRetransmission = DISABLE;
  hfdcan1.Init.TransmitPause = DISABLE;
  hfdcan1.Init.ProtocolException = DISABLE;
  //时钟频率为60MHz，CAN波特率配置为500KBps
  hfdcan1.Init.NominalPrescaler = 8;
  hfdcan1.Init.NominalSyncJumpWidth = 1;
  hfdcan1.Init.NominalTimeSeg1 = 11;
  hfdcan1.Init.NominalTimeSeg2 = 3;
  hfdcan1.Init.DataPrescaler = 1;
  hfdcan1.Init.DataSyncJumpWidth = 1;
  hfdcan1.Init.DataTimeSeg1 = 1;
  hfdcan1.Init.DataTimeSeg2 = 1;
  hfdcan1.Init.MessageRAMOffset = 0;
  hfdcan1.Init.StdFiltersNbr = 2;
  hfdcan1.Init.ExtFiltersNbr = 2;
  hfdcan1.Init.RxFifo0ElmtsNbr = 5;
  hfdcan1.Init.RxFifo0ElmtSize = FDCAN_DATA_BYTES_8;
  hfdcan1.Init.RxFifo1ElmtsNbr = 0;
  hfdcan1.Init.RxFifo1ElmtSize = FDCAN_DATA_BYTES_8;
  hfdcan1.Init.RxBuffersNbr = 0;
  hfdcan1.Init.RxBufferSize = FDCAN_DATA_BYTES_8;
  hfdcan1.Init.TxEventsNbr = 0;
  hfdcan1.Init.TxBuffersNbr = 0;
  hfdcan1.Init.TxFifoQueueElmtsNbr = 1;
  hfdcan1.Init.TxFifoQueueMode = FDCAN_TX_FIFO_OPERATION;
  hfdcan1.Init.TxElmtSize = FDCAN_DATA_BYTES_8;
  hfdcan1.msgRam.StandardFilterSA = 0;
  hfdcan1.msgRam.ExtendedFilterSA = 0;
  hfdcan1.msgRam.RxFIFO0SA = 0;
  hfdcan1.msgRam.RxFIFO1SA = 0;
  hfdcan1.msgRam.RxBufferSA = 0;
  hfdcan1.msgRam.TxEventFIFOSA = 0;
  hfdcan1.msgRam.TxBufferSA = 0;
  hfdcan1.msgRam.TxFIFOQSA = 0;
  hfdcan1.msgRam.TTMemorySA = 0;
  hfdcan1.msgRam.EndAddress = 0;
  if (HAL_FDCAN_Init(&hfdcan1) != HAL_OK)
  {
    printf("HAL_FDCAN_Init Error!\r\n");
    Error_Handler();
  }
  //设置CAN接收过滤器
  CAN_ConfigFilter(Channel,0,PHY_ADDR_ID,MSG_ID_TYPE);
  CAN_ConfigFilter(Channel,1,FUN_ADDR_ID,MSG_ID_TYPE);
  /* Configure global filter:
     Filter all remote frames with STD and EXT ID
     Reject non matching frames with STD ID and EXT ID */
  if (HAL_FDCAN_ConfigGlobalFilter(&hfdcan1, FDCAN_REJECT, FDCAN_REJECT, ENABLE, ENABLE) != HAL_OK)
  {
    printf("HAL_FDCAN_ConfigGlobalFilter Error!\r\n");
    Error_Handler();
  }

  /* Start the FDCAN module */
  if (HAL_FDCAN_Start(&hfdcan1) != HAL_OK)
  {
    printf("HAL_FDCAN_Start Error!\r\n");
    Error_Handler();
  }

  if (HAL_FDCAN_ActivateNotification(&hfdcan1, FDCAN_IT_RX_FIFO0_NEW_MESSAGE, 0) != HAL_OK)
  {
    printf("HAL_FDCAN_ActivateNotification Error!\r\n");
    Error_Handler();
  }
}


/**
  * @brief  发送一帧CAN数据
  * @param  CANx CAN通道号
	* @param  TxMessage CAN消息指针
  * @retval 1-消息发送成功，0-消息发送失败
  */
uint8_t CAN_WriteData(uint8_t Channel,CAN_MSG *pMsg)
{
  uint32_t  TimeOut=0;
  FDCAN_TxHeaderTypeDef FDCAN_Tx;
  FDCAN_Tx.Identifier=pMsg->ID;
  if(pMsg->Flags.IDE){
    FDCAN_Tx.IdType=FDCAN_EXTENDED_ID;
  }else{
    FDCAN_Tx.IdType=FDCAN_STANDARD_ID;
  }
  FDCAN_Tx.TxFrameType=FDCAN_DATA_FRAME;
  FDCAN_Tx.DataLength=FDCAN_DLC_BYTES_8;
  FDCAN_Tx.ErrorStateIndicator=FDCAN_ESI_ACTIVE;
  FDCAN_Tx.BitRateSwitch=FDCAN_BRS_OFF;
  FDCAN_Tx.FDFormat=FDCAN_CLASSIC_CAN;
  FDCAN_Tx.TxEventFifoControl=FDCAN_NO_TX_EVENTS;
  FDCAN_Tx.MessageMarker=0;
  while(HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan1,&FDCAN_Tx,pMsg->Data)!=HAL_OK){
    TimeOut++;
    if(TimeOut > 100){
      return 0;
    }
    HAL_Delay(1);
  }
  //HAL_Delay(5);
  return 1;
}
/**
  * @brief  CAN接收中断处理函数
  * @param  None
  * @retval None
  */
void HAL_FDCAN_RxFifo0Callback(FDCAN_HandleTypeDef *hfdcan, uint32_t RxFifo0ITs)
{
  if((RxFifo0ITs & FDCAN_IT_RX_FIFO0_NEW_MESSAGE)!=RESET)   //FIFO1新数据中断
  {
    if(hfdcan == &hfdcan1)//CANFD1中断
    {
      CAN_MSG *pRxMsg = CAN_BUF_PickMsg(0);
      FDCAN_RxHeaderTypeDef FDCAN_Rx;
      uint8_t can_data[8];
      HAL_FDCAN_GetRxMessage(&hfdcan1,FDCAN_RX_FIFO0,&FDCAN_Rx,pRxMsg->Data);
#if MSG_ID_TYPE
      if((FDCAN_Rx.IdType == FDCAN_EXTENDED_ID)&&((FDCAN_Rx.Identifier == PHY_ADDR_ID)||(FDCAN_Rx.Identifier == FUN_ADDR_ID))){
#else
      if((FDCAN_Rx.IdType == FDCAN_STANDARD_ID)&&((FDCAN_Rx.Identifier == PHY_ADDR_ID)||(FDCAN_Rx.Identifier == FUN_ADDR_ID))){
#endif
        pRxMsg->Flags.IDE = (FDCAN_Rx.IdType == FDCAN_STANDARD_ID)?0:1;
        pRxMsg->Flags.RTR = 0;
        pRxMsg->Flags.DIR = 0;
        pRxMsg->ID = FDCAN_Rx.Identifier;
        pRxMsg->Flags.DLC = FDCAN_Rx.DataLength>>16;
        CAN_BUF_ApendMsg(0);
        //printf("Got CAN data\r\n");
      }
      HAL_FDCAN_ActivateNotification(&hfdcan1,FDCAN_IT_RX_FIFO0_NEW_MESSAGE,0);
    }
  }
}

/*********************************END OF FILE**********************************/
