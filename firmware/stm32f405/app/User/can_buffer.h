/**
 * @file   can_buffer.h
 * @brief  CAN消息FIFO缓冲区
 * @author wdluo(wdluo@toomoss.com)
 * @version 1.0
 * @date 2022-10-18
 * @copyright Copyright (c) 2022 重庆图莫斯电子科技有限公司
 */
#ifndef __CAN_BUFFER_H
#define __CAN_BUFFER_H

#include <stdint.h>
#include "can_driver.h"


/**
 * @brief 消息缓冲区控制结构体 
 */
typedef struct _CAN_BUFFER{
  int  WriteIndex;  ///<当前正在接收数据的帧索引
  int  ReadIndex;   ///<完整帧，读数据起始索引
  int  GotMsgNum;   ///<接收到的完整帧数，这些帧可以被上位机读取走
  int  BufferSize;  ///<缓冲区能装下的最大帧数
  CAN_MSG *pBuffer; ///<缓冲区首地址
}CAN_BUFFER;

/**
 * @brief FIFO缓冲区初始化 
 * @param Channel CAN通道号
 */
void CAN_BUF_Init(uint8_t Channel);
/**
 * @brief  获取当前正在接收数据的帧指针
 * @param  Channel CAN通道号
 * @return 当前正在接收帧的指针
 */
CAN_MSG *CAN_BUF_PickMsg(uint8_t Channel);

/**
 * @brief  增加1帧CAN消息到FIFO
 * @param  Channel CAN通道号
 */
void CAN_BUF_ApendMsg(uint8_t Channel);

/**
 * @brief  增加1帧CAN消息到FIFO
 * @param  Channel CAN通道号
 * @param  CANMsg CAN消息
 */
void CAN_BUF_PushMsg(uint8_t Channel,CAN_MSG CANMsg);

/**
 * @brief  从FIFO中获取CAN消息
 * @param  Channel CAN通道0029C51D.png
 * @param  pMsg CAN消息指针
 * @param  MsgLen 获取CAN消息帧数
 * @return 实际获取到的CAN消息帧数
 */
int CAN_BUF_PopMsg(uint8_t Channel,CAN_MSG **pMsg,uint8_t MsgLen);

#endif
